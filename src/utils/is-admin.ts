import { Admins } from '../enum/admins.enum';

export function isAdmin(userId: number | undefined): boolean {
  return userId ? Object.values(Admins).includes(`${userId}` as any as Admins) : false;
}
