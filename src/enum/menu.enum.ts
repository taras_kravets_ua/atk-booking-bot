export enum MenuEnum {
  BOOK = 'Занять корт',
  REMOVE_BOOKING = 'Удалить бронь',
  CHECK_COURTS = 'Проверить кто занимал',
  PROFILE = 'Профиль',
  TODAY = 'Сегодня',
  TOMORROW = 'Завтра',
  BACK_TO_MENU = 'Назад в меню',
  FIRST_HALF_OF_DAY = 'Первая половина дня',
  SECOND_HALF_OF_DAY = 'Вторая половина дня',
  ADMIN = 'Админ',
  ADMIN_REMOVE_BOOKING = 'Удалить бронирование'
}
