export const DurationEnum = {
  HALF: '0.5 часа',
  HOUR: '1 час',
  HOUR_AND_HALF: '1.5 часа',
  TWO_HOURS: '2 часа',
  TWO_HOURS_AND_HALF: '2.5 часа',
  THREE_HOURS: '3 часа',
} as const;
