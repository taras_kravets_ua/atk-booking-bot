export const MinutesEnum = {
  ZERO: '00',
  TEN: '10',
  TWENTY: '20',
  THIRTY: '30',
  FORTY: '40',
  FIFTY: '50',
} as const;

