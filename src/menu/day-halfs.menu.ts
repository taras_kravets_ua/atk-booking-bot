import { Context, Markup } from 'telegraf';
import { MenuEnum } from '../enum/menu.enum';
import { MessageEnum } from '../enum/message.enum';
import { DayType } from '../types/state.type';
import { StateService } from '../services/state.service';

export const showPartsOfDayMenu = (ctx: Context, day: DayType) => {
  const state = new StateService();
  state.update({ day });

  return ctx.reply(MessageEnum.START_TIME, Markup
      .keyboard([
        [MenuEnum.FIRST_HALF_OF_DAY, MenuEnum.SECOND_HALF_OF_DAY],
        [MenuEnum.BACK_TO_MENU]
      ])
      .oneTime()
      .resize()
  )
}
