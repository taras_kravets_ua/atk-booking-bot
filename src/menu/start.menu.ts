import { Context, Markup } from 'telegraf';
import { MenuEnum } from '../enum/menu.enum';
import { MessageEnum } from '../enum/message.enum';
import { isAdmin } from '../utils/is-admin';

export const showStartMenu = (ctx: Context) => {
  if (!ctx.from?.username) {
    ctx.reply(MessageEnum.ADD_USERNAME);
  }

  if (`${ctx.chat?.id}` === `${process.env.ATK_GROUP_ID}`) {
    return ctx.reply(MessageEnum.NOPE, Markup.removeKeyboard());
  }

  return ctx.reply(MessageEnum.START_MESSAGE, Markup
      .keyboard([
        [MenuEnum.BOOK, MenuEnum.REMOVE_BOOKING],
        [MenuEnum.CHECK_COURTS, isAdmin(ctx.from?.id) ? MenuEnum.ADMIN : '']
      ])
      .oneTime()
      .resize()
  )
}
