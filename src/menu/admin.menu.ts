import { Context, Markup } from 'telegraf';
import { MessageEnum } from '../enum/message.enum';
import { MenuEnum } from '../enum/menu.enum';

export const showAdminMenu = (ctx: Context) => {
  return ctx.reply(MessageEnum.ADMIN_MENU, Markup.keyboard([
    [MenuEnum.ADMIN_REMOVE_BOOKING],
    [MenuEnum.BACK_TO_MENU]
  ])
      .oneTime()
      .resize())
}
