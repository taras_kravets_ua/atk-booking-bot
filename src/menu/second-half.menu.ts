import { MessageEnum } from '../enum/message.enum';
import { Context, Markup } from 'telegraf';
import { TimeEnum } from '../enum/time.enum';
import { MenuEnum } from '../enum/menu.enum';

export const showSecondHalfOfDayMenu = (ctx: Context) => {
  return ctx.reply(MessageEnum.WHAT_HOUR, Markup
      .keyboard([
        [TimeEnum.FOURTEEN, TimeEnum.FIFTEEN, TimeEnum.SIXTEEN, TimeEnum.SEVENTEEN],
        [TimeEnum.EIGHTEEN, TimeEnum.NINETEEN, TimeEnum.TWENTY, TimeEnum.TWENTY_ONE],
        [MenuEnum.BACK_TO_MENU]
      ])
      .oneTime()
      .resize()
  )
}
