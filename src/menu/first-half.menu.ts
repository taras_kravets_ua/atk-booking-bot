import { MessageEnum } from '../enum/message.enum';
import { Context, Markup } from 'telegraf';
import { TimeEnum } from '../enum/time.enum';
import { MenuEnum } from '../enum/menu.enum';

export const showFirstHalfOfDayMenu = (ctx: Context) => {
  return ctx.reply(MessageEnum.WHAT_HOUR, Markup
      .keyboard([
        [TimeEnum.SIX, TimeEnum.SEVEN, TimeEnum.EIGHT, TimeEnum.NINE],
        [TimeEnum.TEN, TimeEnum.ELEVEN, TimeEnum.TWELVE, TimeEnum.THIRTEEN],
        [MenuEnum.BACK_TO_MENU]
      ])
      .oneTime()
      .resize()
  )
}
