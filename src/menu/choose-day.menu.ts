import { Context, Markup } from 'telegraf';
import { MessageEnum } from '../enum/message.enum';
import { MenuEnum } from '../enum/menu.enum';

export const showChooseDayMenu = (ctx: Context) => {
  return ctx.reply(MessageEnum.CHOOSE_DAY, Markup.keyboard([
    [MenuEnum.TODAY, MenuEnum.TOMORROW],
    [MenuEnum.BACK_TO_MENU]
  ])
      .oneTime()
      .resize())
}
