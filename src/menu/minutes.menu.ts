import { MessageEnum } from '../enum/message.enum';
import { Context, Markup } from 'telegraf';
import { MinutesEnum } from '../enum/minutes.enum';
import { MenuEnum } from '../enum/menu.enum';

export const showMinutesMenu = (ctx: Context, hour: string) => {
  return ctx.reply(MessageEnum.START_TIME, Markup
      .keyboard([
        [`${hour}:${MinutesEnum.ZERO}`, `${hour}:${MinutesEnum.TEN}`, `${hour}:${MinutesEnum.TWENTY}`],
        [`${hour}:${MinutesEnum.THIRTY}`, `${hour}:${MinutesEnum.FORTY}`, `${hour}:${MinutesEnum.FIFTY}`],
        [MenuEnum.BACK_TO_MENU]
      ])
      .oneTime()
      .resize()
  )
}
