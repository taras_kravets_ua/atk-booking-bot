import { MessageEnum } from '../enum/message.enum';
import { Context, Markup } from 'telegraf';
import { DurationEnum } from '../enum/duration.enum';
import { StateService } from '../services/state.service';
import { MenuEnum } from '../enum/menu.enum';

export const showDurationMenu = (ctx: Context, startTime: string) => {
  const state = new StateService();
  state.update({ startTime });

  return ctx.reply(MessageEnum.DURATION, Markup
      .keyboard([
        [DurationEnum.HALF, DurationEnum.HOUR, DurationEnum.HOUR_AND_HALF],
        [DurationEnum.TWO_HOURS, DurationEnum.TWO_HOURS_AND_HALF, DurationEnum.THREE_HOURS],
        [MenuEnum.BACK_TO_MENU]
      ])
      .oneTime()
      .resize()
  )
}
