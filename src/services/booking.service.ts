import { BookingEntity, BookingItem, BookingRange } from '../types/booking.type';
import { Firestore, collection, getDocs, setDoc, doc, deleteDoc } from '@firebase/firestore/lite';
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore/lite';
import { FirebaseOptions } from '@firebase/app';
import { DateService } from './date-service';
import { Context, Markup, Telegraf } from 'telegraf';
import { Update } from 'typegram';
import { MessageEnum } from '../enum/message.enum';
import { showStartMenu } from '../menu/start.menu';
import { isAdmin } from '../utils/is-admin';

const firebaseConfig: FirebaseOptions = {
  apiKey: process.env.FIREBASE_API_TOKEN,
  projectId: process.env.FIREBASE_PROJECT_ID,
  authDomain: `${process.env.FIREBASE_PROJECT_ID}.firebaseapp.com`,
  databaseURL: process.env.FIREBASE_DB_URL,
  storageBucket: `${process.env.FIREBASE_PROJECT_ID}.appspot.com`,
  messagingSenderId: process.env.FIREBASE_SENDER_ID
};

export class BookingService {
  private static instance: BookingService;
  private db!: Firestore;

  constructor() {
    if (!BookingService.instance) {
      BookingService.instance = this;
    }
    return BookingService.instance;
  }

  initFirestore(): void {
    const app = initializeApp(firebaseConfig);
    this.db = getFirestore(app);
  }

  async getAllBookingsByDate(date: string): Promise<BookingEntity[]> {
    const bookings = collection(this.db, date);
    const snapshot = await getDocs(bookings);
    return snapshot.docs.map(doc => doc.data()) as BookingEntity[];
  }

  async deleteBookingByDate(date: string) {
    const ds = new DateService();
    await deleteDoc(doc(this.db, ds.getDateFromISO(date), date));
  }

  async createBooking({ date, lastName, bookingRange, firstName, userName, userId }: BookingItem): Promise<void> {
    await setDoc(doc(this.db, date, bookingRange.from), {
      lastName,
      bookingRange,
      firstName,
      userName,
      userId
    });
  }

  buildBookingList(bookings: BookingEntity[]): string {
    const ds = new DateService();
    let list = `${ds.getDateFromISO(bookings[0].bookingRange.from)}\n`;
    bookings.sort().forEach(booking => {
      const userName = booking.userName ? `(@${booking.userName})` : '';
      const extra = this.createExtraString(booking.userName);
      list += `${ds.getFormattedRange(booking.bookingRange)} ${booking.firstName || ''} ${booking.lastName || ''} ${userName} ${extra}\n`
    })

    return list;
  }

  createExtraString(username: string): string {
    switch (username) {
      case 'Golovkov':
        return '🎾';
      case 'olegtriathlon':
        return '🚴';
      default:
        return '';
    }
  }

  getBookingsByUserId(userId: number): Promise<[BookingEntity[], BookingEntity[]]> {
    return this.getTodayAndTomorrowBookings()
      .then(([todayBookings, tomorrowBookings]) => {
        const today = todayBookings.filter(booking => booking.userId === userId);
        const tomorrow = tomorrowBookings.filter(booking => booking.userId === userId);
        return [today, tomorrow];
    })
  }

  getTodayAndTomorrowBookings(): Promise<[BookingEntity[], BookingEntity[]]> {
    const ds = new DateService();

    const today = ds.getFormattedDay('today');
    const tomorrow = ds.getFormattedDay('tomorrow');

    return Promise.all([
      this.getAllBookingsByDate(today),
      this.getAllBookingsByDate(tomorrow)
    ])
  }

  buildRemoveButtons(
      ctx: Context,
      bookings: BookingEntity[],
      bot: Telegraf<Context<Update>>,
  ) {
    const ds = new DateService();

    bookings.forEach(booking => {
      const {firstName, userName, bookingRange, lastName} = booking;
      const name = isAdmin(ctx.from?.id)
          ? `${firstName || '' } ${lastName || '' } ${userName ? '(@' + userName + ')' : '' }`
          : '';

      ctx.reply(
          `${ds.getDateFromISO(bookingRange.from)} на ${ds.getFormattedRange(bookingRange)} ${name}`,
          Markup.inlineKeyboard([
                {
                  text: 'Удалить',
                  callback_data: `delete-${bookingRange.from}`
                },
              ]
          )
      )

      bot.action(`delete-${bookingRange.from}`, ctx => {
        this.deleteBookingByDate(bookingRange.from).then(() => {
          ctx.reply(MessageEnum.BOOKING_REMOVED);
          ctx.telegram.sendMessage(
              process.env.ATK_GROUP_ID!,
              `${isAdmin(ctx.from?.id) ? MessageEnum.ADMIN_REMOVED_BOOKING : MessageEnum.USER_REMOVED_BOOKING}\n${this.buildBookingList([booking])}`
          )
          return showStartMenu(ctx);
        })
      })
    })
  }

  async isTimeAvailable(date: string, bookingRange: BookingRange): Promise<boolean> {
    const ds = new DateService();
    const bookings = await this.getAllBookingsByDate(date);

    return !bookings.some(booking => ds.isTimeCrossing(bookingRange, booking.bookingRange));
  }
}
