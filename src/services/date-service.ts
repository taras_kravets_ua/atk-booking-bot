import moment, { ISO_8601 } from 'moment';
import { DurationValue } from '../types/enum-types.type';
import { BookingRange } from '../types/booking.type';
import { StateService } from './state.service';
import { DayType } from '../types/state.type';

const COLLECTION_KEY_FORMAT = 'DD-MM-YYYY';
const TIME_FORMAT = 'HH:mm';

export class DateService {
  getMinutesFromDuration(duration: DurationValue): number {
    return +(duration.split(' ')[0]) * 60;
  }

  createTimeRange(duration: number): BookingRange {
    const state = new StateService();

    let from = moment(state.get().startTime, TIME_FORMAT).format();

    if (state.get().day === 'tomorrow') {
      from = moment(from, ISO_8601).add(1, 'day').format();
    }

    const to = moment(from, ISO_8601).add(duration, 'minutes').format();

    return { from, to };
  }

  getDateFromISO(isoDate: string) {
    return moment(isoDate, ISO_8601).format(COLLECTION_KEY_FORMAT);
  }

  getFormattedDay(day: DayType): string {
    switch (day) {
      case 'today':
        return moment().format(COLLECTION_KEY_FORMAT);
      case 'tomorrow':
        return moment().add(1, 'day').format(COLLECTION_KEY_FORMAT);
    }
  }

  getFormattedRange(range: BookingRange): string {
    return `${this.getTimeFromDate(range.from)} - ${this.getTimeFromDate(range.to)}`;
  }

  isTimeCrossing(range: BookingRange, bookedRange: BookingRange): boolean {
    return this.isCrossing(range, bookedRange) || this.isCrossing(bookedRange, range);
  }

  private isCrossing(firstRange: BookingRange, secondRange: BookingRange): boolean {
    return moment(firstRange.from, ISO_8601).isBetween(
            moment(secondRange.from, ISO_8601),
            moment(secondRange.to, ISO_8601),
            'minutes',
            '()'
        ) ||
        moment(firstRange.to, ISO_8601).isBetween(
            moment(secondRange.from, ISO_8601),
            moment(secondRange.to, ISO_8601),
            'minutes',
            '()'
        );
  }

  private getTimeFromDate(date: string): string {
    return moment(date, ISO_8601).format(TIME_FORMAT);
  }
}
