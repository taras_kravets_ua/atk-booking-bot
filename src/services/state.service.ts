import { BookingState } from '../types/state.type';

export class StateService {
  private static instance: StateService | null;
  private state: BookingState = {};

  constructor() {
    if (!StateService.instance) {
      StateService.instance = this;
    }
    return StateService.instance;
  }

  get(): BookingState {
    return this.state;
  }

  update(state: BookingState): void {
    this.state = { ...this.state, ...state }
  }

  clean(): void {
    StateService.instance = null;
  }
}
