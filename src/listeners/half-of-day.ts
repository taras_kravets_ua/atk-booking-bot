import { MenuEnum } from '../enum/menu.enum';
import { showFirstHalfOfDayMenu } from '../menu/first-half.menu';
import { showSecondHalfOfDayMenu } from '../menu/second-half.menu';
import { Context, Telegraf } from 'telegraf';
import { Update } from 'typegram';

export function initHalfOfDayListeners(bot: Telegraf<Context<Update>>) {
  bot.hears(MenuEnum.FIRST_HALF_OF_DAY, ctx => showFirstHalfOfDayMenu(ctx));
  bot.hears(MenuEnum.SECOND_HALF_OF_DAY, ctx => showSecondHalfOfDayMenu(ctx));
}
