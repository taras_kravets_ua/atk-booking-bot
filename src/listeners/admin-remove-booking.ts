import { Context, Telegraf } from 'telegraf';
import { Update } from 'typegram';
import { MenuEnum } from '../enum/menu.enum';
import { BookingService } from '../services/booking.service';

export function initAdminRemoveBookingListeners(bot: Telegraf<Context<Update>>) {
  bot.hears(MenuEnum.ADMIN_REMOVE_BOOKING, async ctx => {
    const bs = new BookingService();
    const [today, tomorrow] = await bs.getTodayAndTomorrowBookings();

    if (today.length) {
      bs.buildRemoveButtons(ctx, today, bot);
    }

    if (tomorrow.length) {
      bs.buildRemoveButtons(ctx, tomorrow, bot);
    }
  })
}
