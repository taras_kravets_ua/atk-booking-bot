import { Context, Telegraf } from 'telegraf';
import { Update } from 'typegram';
import { MenuEnum } from '../enum/menu.enum';
import { BookingService } from '../services/booking.service';

export function initRemoveListener(bot: Telegraf<Context<Update>>) {
  bot.hears(MenuEnum.REMOVE_BOOKING, async ctx => {
    const bs = new BookingService();
    const [today, tomorrow] = await bs.getBookingsByUserId(ctx.from.id);

    if (today.length) {
      bs.buildRemoveButtons(ctx, today, bot);
    }

    if (tomorrow.length) {
      bs.buildRemoveButtons(ctx, tomorrow, bot);
    }
  });
}
