import { Context, Telegraf } from 'telegraf';
import { Update } from 'typegram';
import { initMainMenuListeners } from './main-menu';
import { initHalfOfDayListeners } from './half-of-day';
import { initHourPickListeners } from './hour-pick';
import { initMinutesPickListeners } from './minutes-pick';
import { initDayListeners } from './day-pick';
import { initDurationListeners } from './duration';
import { initRemoveListener } from './remove-booking';
import { initAdminListeners } from './admin';
import { initAdminRemoveBookingListeners } from './admin-remove-booking';

export function initAllListeners(bot: Telegraf<Context<Update>>) {
  initMainMenuListeners(bot);
  initDayListeners(bot);
  initHalfOfDayListeners(bot);
  initHourPickListeners(bot);
  initMinutesPickListeners(bot);
  initDurationListeners(bot);
  initRemoveListener(bot);
  initAdminListeners(bot);
  initAdminRemoveBookingListeners(bot);
}
