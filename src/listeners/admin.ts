import { Context, Telegraf } from 'telegraf';
import { Update } from 'typegram';
import { MenuEnum } from '../enum/menu.enum';
import { showAdminMenu } from '../menu/admin.menu';

export function initAdminListeners(bot: Telegraf<Context<Update>>) {
  bot.hears(MenuEnum.ADMIN, ctx => showAdminMenu(ctx))
}
