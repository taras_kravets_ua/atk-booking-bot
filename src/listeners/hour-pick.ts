import { Context, Telegraf } from 'telegraf';
import { Update } from 'typegram';
import { TimeEnum } from '../enum/time.enum';
import { showMinutesMenu } from '../menu/minutes.menu';

export function initHourPickListeners(bot: Telegraf<Context<Update>>) {
  Object.values(TimeEnum).forEach(time => {
    const hour = time.split('-')[0];
    bot.hears(time, ctx =>
        showMinutesMenu(ctx, hour));
  })
}
