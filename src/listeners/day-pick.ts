import { Context, Telegraf } from 'telegraf';
import { Update } from 'typegram';
import { MenuEnum } from '../enum/menu.enum';
import { showPartsOfDayMenu } from '../menu/day-halfs.menu';

export function initDayListeners(bot: Telegraf<Context<Update>>) {
  bot.hears(MenuEnum.TODAY, ctx =>
      showPartsOfDayMenu(ctx, 'today'));

  bot.hears(MenuEnum.TOMORROW, ctx =>
      showPartsOfDayMenu(ctx, 'tomorrow'));
}
