import { Context, Telegraf } from 'telegraf';
import { MenuEnum } from '../enum/menu.enum';
import { Update } from 'typegram';
import { showChooseDayMenu } from '../menu/choose-day.menu';
import { BookingService } from '../services/booking.service';
import { DateService } from '../services/date-service';
import { showStartMenu } from '../menu/start.menu';
import { StateService } from '../services/state.service';
import { MessageEnum } from '../enum/message.enum';

export function initMainMenuListeners(bot: Telegraf<Context<Update>>) {
  bot.hears(MenuEnum.BOOK, ctx =>
      showChooseDayMenu(ctx));

  bot.hears(MenuEnum.CHECK_COURTS, async (ctx) => {
    const bs = new BookingService();
    const ds = new DateService();

    const today = await bs.getAllBookingsByDate(ds.getFormattedDay('today'));
    if (today?.length) {
      ctx.reply(bs.buildBookingList(today));
    }

    const tomorrow = await bs.getAllBookingsByDate(ds.getFormattedDay('tomorrow'));
    if (tomorrow?.length) {
      ctx.reply(bs.buildBookingList(tomorrow));
    }

    if (!today.length && !tomorrow.length) {
      ctx.reply(MessageEnum.YOU_WILL_BE_FIRST);
    }
  });

  bot.hears(MenuEnum.BACK_TO_MENU, ctx => {
    const state = new StateService();
    state.clean();
    return showStartMenu(ctx)
  });
}
