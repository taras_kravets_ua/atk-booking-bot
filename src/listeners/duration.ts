import { Context, Telegraf } from 'telegraf';
import { Update } from 'typegram';
import { DurationEnum } from '../enum/duration.enum';
import { StateService } from '../services/state.service';
import { DateService } from '../services/date-service';
import { BookingService } from '../services/booking.service';
import { showStartMenu } from '../menu/start.menu';
import { MessageEnum } from '../enum/message.enum';

export function initDurationListeners(bot: Telegraf<Context<Update>>) {
  Object.values(DurationEnum).forEach(duration => {
    bot.hears(duration, async (ctx) => {
      const state = new StateService();
      const ds = new DateService();
      const durationInMinutes = ds.getMinutesFromDuration(duration);
      state.update({durationInMinutes});

      const range = ds.createTimeRange(durationInMinutes);
      const date = ds.getDateFromISO(range.from);
      // My Debugger xD
      // console.log('----------------------------------------');
      // console.log(range, '<<<<<<<<<<<<<<<<<<<<<<<<<<<<< RANGE');
      // console.log(date, '<<<<<<<<<<<<<<<<<<<<<<<<<<<<< DATE');
      // console.log(ctx.from, '<<<<<<<<<<<<<<<<<<<<<<<<<<<< CONTEX');
      // console.log('----------------------------------------');

      const bs = new BookingService();
      if (await bs.isTimeAvailable(date, range)) {
        const newBooking = {
          firstName: ctx.from.first_name || '',
          lastName: ctx.from.last_name || '',
          userName: ctx.from.username || '',
          bookingRange: range,
          date,
          userId: ctx.from.id
        }
        await bs.createBooking(newBooking).then(() => {
          ctx.reply(MessageEnum.BOOKING_SUCCESS);
          const bs = new BookingService();
          ctx.telegram.sendMessage(
              process.env.ATK_GROUP_ID!,
              `${MessageEnum.NEW_BOOKING}\n${bs.buildBookingList([newBooking])}`
          )
          state.clean();
          showStartMenu(ctx);
        });
      } else {
        ctx.reply(MessageEnum.TIME_CROSSING_ERROR);
        state.clean();
        return showStartMenu(ctx);
      }
    })
  })
}
