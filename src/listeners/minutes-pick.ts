import { Context, Telegraf } from 'telegraf';
import { Update } from 'typegram';
import { TimeEnum } from '../enum/time.enum';
import { MinutesEnum } from '../enum/minutes.enum';
import { showDurationMenu } from '../menu/duration.menu';

export function initMinutesPickListeners(bot: Telegraf<Context<Update>>) {
  Object.values(TimeEnum).forEach(time => {
    Object.values(MinutesEnum).forEach(minutes => {
      const hour = time.split('-')[0];
      const startTime = `${hour}:${minutes}`
      bot.hears(startTime, ctx => showDurationMenu(ctx, startTime));
    })
  })
}
