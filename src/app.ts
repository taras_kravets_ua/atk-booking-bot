import 'dotenv/config';
import { Context, Telegraf } from 'telegraf';
import { Update } from 'typegram';
import { showStartMenu } from './menu/start.menu';
import { initAllListeners } from './listeners/init-all-listeners';
import { BookingService } from './services/booking.service';

const bs = new BookingService();
bs.initFirestore();

const bot: Telegraf<Context<Update>> = new Telegraf(process.env.BOT_TOKEN as string);

bot.start(ctx => showStartMenu(ctx));

initAllListeners(bot);

bot.launch();
