export type DayType = 'today' | 'tomorrow';
export type HalfType = 'first' | 'second';

export type BookingState = {
  day?: DayType;
  half?: HalfType;
  startTime?: string;
  durationInMinutes?: number;
}
