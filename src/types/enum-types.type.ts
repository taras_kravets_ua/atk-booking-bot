import { TimeEnum } from '../enum/time.enum';
import { MinutesEnum } from '../enum/minutes.enum';
import { DurationEnum } from '../enum/duration.enum';

export type TimeValue = typeof TimeEnum[keyof typeof TimeEnum];
export type MinuteValue = typeof MinutesEnum[keyof typeof MinutesEnum];
export type DurationValue = typeof DurationEnum[keyof typeof DurationEnum];
