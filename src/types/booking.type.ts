export type BookingItem = {
  bookingRange: BookingRange;
  userName: string;
  firstName?: string;
  lastName?: string;
  date: string;
  userId: number;
}

export type BookingRange = {
  from: string,
  to: string,
}

export type BookingEntity = Omit<BookingItem, 'date'>;
